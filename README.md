ДЗ 11.1

Предположим, есть класс.

        public class Foo {
        public void first() { print("first"); }
        public void second() { print("second"); }
        public void third() { print("third"); }
    }

Один и тот же экземпляр класса Foo будет передаваться трем разным потокам. Поток А будет вызывать first (), поток В 
second (), поток C third (). Разработайте механизм, чтобы обеспечить выполнение сначала first (), second () после 
first (), а third () после second ().

    Пример 1
    Входные данные: [1,2,3]
    Результат: "firstsecondthird"
    Три потока вызваны асинхронно. Входные данные [1,2,3] означает, что поток А вызывает first(), поток B second() 
    и поток C third().

    Пример 2
    [1,3,2] -> "firstsecondthird"
    Поток А вызывает first (), B вызывает third (), поток С second ().

Пример структуры

       class Foo {
    
         public Foo() {
                
            }
        
            public void first(Runnable printFirst) throws InterruptedException {
                printFirst.run();
            }
        
            public void second(Runnable printSecond) throws InterruptedException {
                printSecond.run();
            }
        
            public void third(Runnable printThird) throws InterruptedException {        
                printThird.run();
            }
            
          } 
          
        }   

