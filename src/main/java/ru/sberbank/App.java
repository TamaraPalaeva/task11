package ru.sberbank;

import ru.sberbank.thread.ThreadFirst;
import ru.sberbank.thread.ThreadSecond;
import ru.sberbank.thread.ThreadThird;

public class App {

    public static volatile int flagThread;
    public static volatile int flagFoo;
    public static int[] threadQueue;

    public static void main(String[] args) throws InterruptedException {
        Object monitor = new Object();
        threadQueue = new int[]{3, 1, 2};
        Foo foo = new Foo();
        ThreadFirst th1 = new ThreadFirst(monitor, foo);
        ThreadSecond th2 = new ThreadSecond(monitor, foo);
        ThreadThird th3 = new ThreadThird(monitor, foo);

        th1.start();
        th2.getThread().start();
        th3.getThread().start();

        flagFoo = 0;
        flagThread = threadQueue[0];

        try {
            th1.join();
            th2.getThread().join();
            th3.getThread().join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    }
}
