package ru.sberbank.thread;

import ru.sberbank.App;
import ru.sberbank.Foo;

public class ThreadFirst extends Thread {
    private Object monitor;
    private Foo foo;


    public ThreadFirst(Object monitor, Foo foo) {
        this.monitor = monitor;
        this.foo = foo;
    }

    public void run() {
        try {
                synchronized (monitor) {
                    while (App.flagThread != 1) {
                        monitor.wait();
                    }

                    if (App.flagFoo == 0) {
                        foo.first(this);
                    } else if (App.flagFoo == 1) {
                        foo.second(this);
                    } else if (App.flagFoo == 2) {
                        foo.third(this);
                    }
                    if (App.flagFoo < 2 ){
                        App.flagFoo++;
                        App.flagThread = App.threadQueue[App.flagFoo];
                        monitor.notifyAll();}
                        monitor.wait();
                }

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
