package ru.sberbank.thread;

import ru.sberbank.App;
import ru.sberbank.Foo;

public class ThreadThird implements Runnable{
    private Object monitor;
    private Thread thread;
    private Foo foo;

    public Thread getThread () {
        return thread;
    }

    public ThreadThird(Object monitor, Foo foo) {
        this.monitor = monitor;
        this.foo = foo;
        thread = new Thread (this);
    }

    public void run() {
        try {
            Thread.sleep(1);
            synchronized (monitor) {
                while (App.flagThread != 3) {
                    monitor.wait();
                }
                if (App.flagFoo == 0) {
                    foo.first(thread);
                } else if (App.flagFoo == 1) {
                    foo.second(thread);
                } else if (App.flagFoo == 2) {
                    foo.third(thread);
                }
                if (App.flagFoo < 2 ){
                    App.flagFoo++;
                    App.flagThread = App.threadQueue[App.flagFoo];
                    monitor.notifyAll();
                }
                monitor.wait();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}