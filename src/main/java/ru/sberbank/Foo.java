package ru.sberbank;

public class Foo {

    public Foo() {
    }

    public void first(Runnable printFirst) throws InterruptedException {
        System.out.print(("first"));
    }

    public void second(Runnable printSecond) throws InterruptedException {
        System.out.print("second");
    }

    public void third(Runnable printThird) throws InterruptedException {
        System.out.print("third");
    }
}
